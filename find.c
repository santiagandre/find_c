#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
int isDirectory(char *path) {//resive un path
   struct stat *statbuf;
   int result;
   
   statbuf = malloc(sizeof(struct stat));//reserva memoria
   if (stat(path, statbuf) != 0){//no existe el path
       return -1;
   }
   result = S_ISDIR(statbuf->st_mode);// retorna 1 si es un directorio
   free(statbuf);//libera recursos
   return result;
}

int find(char * path, char * name){
//pre: se asume que la ruta es de un directorio
	int cont = 0;
	int len_path = strlen(path);
	int len_subpath;
	char * sub_path;
	struct dirent *dirent;
	DIR *dir;
	
	dir = opendir (path); //se abre el directorio
	if(dir == NULL){//validacion de existencia del directorio
		//printf("EL directorio %s no existe\n",path);// la comento porque muchos directorios pueden caer aqui y al usuario lo le aporta nada
		return 0;
	}
	while ((dirent = readdir(dir)) != NULL) {//recorro el contenido del directorio 
		if(strcmp(".",dirent->d_name) == 0 || strcmp("..",dirent->d_name) == 0){// evito un ciclo infinito, ya que . y .. hacen referencia a el mismo directorio y al padre
			continue;// sigo con el siguiente archivo o directorio
		}
		if(strcmp(name,dirent->d_name) == 0){//compara el nombre del archivo actual con el que se esta buscando
			printf ("%s/%s\n", path,name);
			cont++;
		}
		//concatena el path con el nombre del actual archivo
		if(len_path == 1){//el path es el directorio raiz -> path "/"
			len_subpath = strlen(dirent->d_name)+2; //obtiene el tamanio de la memoria del vector
			sub_path = (char*)malloc(len_subpath); //reserva memoria
			snprintf(sub_path,len_subpath, "/%s",dirent->d_name); //copia todo a subpath
		}else{
			len_subpath = len_path+strlen(dirent->d_name)+2; //obtiene el tamanio de la memoria del vector
			sub_path = (char*)malloc(len_subpath); //reserva memoria
			snprintf(sub_path,len_subpath, "%s/%s", path,dirent->d_name); //copia todo a subpath
		}
		if(isDirectory(sub_path) == 1){//es un directorio?
			cont =  cont + find(sub_path,name);//suma todas las veces que aparecio find con las que lleva
		}
	}
	closedir (dir); //cierra el directorio, libera memoria
	return cont;
}

int main(int argc, char* argv[])
{
	char *path;
	char* name;
	int aux;
    //argc mínimo es 1, el nombre del  ejecutable
    // El parámetro dos en mi aplicación tiene que ser el nombre del archivo a buscar
	if(argc!=3){//error 
		printf("error: número de argumentos inválido\n");
		exit(EXIT_FAILURE);
	}
	//el primer parametro es la carpeta en donde se va abuscar el archivo
	path = argv[1]; //un apuntador a la misma direccion de memoria 
	//el segundo parametro es el nombre del archivo con extencion
	name = argv[2];
	printf("path: %s\n",path);
	printf("name: %s\n",name);
	//determino si el path es una ruta existente y es un directorio o no
	aux = isDirectory(path);
	if(aux == -1){//no existe
		printf("error: el path %s no existe\n",path);
		exit(EXIT_FAILURE);
	}
	if (aux == 0){// no es un directorio
		printf("error: el path %s no es un directorio\n",path);
		exit(EXIT_FAILURE);
	}
	//es un  directorio
	printf("Total: %d\n",find(path,name));
	
}
